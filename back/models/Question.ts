import * as mongoose from 'mongoose';
import {IQuestion} from '../interfaces';

const schema:mongoose.Schema = new mongoose.Schema({
    title: {
        type: String
    },
    recommendationLinks: [{
        recommendation: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Recommendation"
        },
        min: Number,
        max: Number
    }]
});



export default mongoose.model<IQuestion>('Question', schema);