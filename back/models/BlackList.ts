import * as mongoose from 'mongoose';
import {IBlackList} from '../interfaces';

const schema:mongoose.Schema = new mongoose.Schema({
    token: {
        type: String,
        unique: true,
        required: true
    }
});



export default mongoose.model<IBlackList>('BlackList', schema);
