import * as mongoose from 'mongoose';
import {IRecommendation} from '../interfaces';

const schema:mongoose.Schema = new mongoose.Schema({
    title: {
        type: String
    },
    ages: {
        type: [String]
    },
    genders: {
        type: [String]
    },
    symptoms: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'StartSymptom'
        }]
    },
    plan: [{
        name: String,
        planType: String
    }]
});



export default mongoose.model<IRecommendation>('Recommendation', schema);
