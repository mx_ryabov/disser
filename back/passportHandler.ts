import passport from "passport";
import passportLocal from "passport-local";
import passportJwt from "passport-jwt";
import User from "./models/User";
import config from "./config";
import { NativeError } from "mongoose";
import { IBaseUser } from "./interfaces";

const LocalStrategy = passportLocal.Strategy;
const JwtStrategy = passportJwt.Strategy;
const ExtractJwt = passportJwt.ExtractJwt;

passport.use(new LocalStrategy({usernameField: "login", passwordField: "password"}, (login:string, password:string, done) => {
    User.findOne({ login: login }).exec((err:NativeError, user:IBaseUser) => {
        if (err) { return done(err); }
        if (!user) {
            return done(undefined, false, { message: `user with login ${login} not found` });
        }
        user.checkPassword(password) ? done(undefined, user) : done(undefined, false, { message: "Invalid login or password" });
    });
}));


passport.use(new JwtStrategy(
    {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: config.session.jwtSecret
    }, function(jwtToken, done) {
        
        User.findOne({ login: jwtToken.login }).exec((err:NativeError, user:IBaseUser) => {
            if (err) { return done(err, false); }
            if (user) {
                return done(undefined, user, jwtToken);
            } else {
                return done(undefined, false);
            }
        });
    }
));