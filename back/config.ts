export default { 
    mongoUrl: "mongodb://localhost/healthhunter",
    session: {
        jwtSecret: 'Tf64576ygvH5fr%$%^&uh',
        name: 'healthhunter.sid',
        proxy: true,
        resave: true,
        saveUninitialized: true,
        //cookie: { secure: true }
        store: {}
    },
    oauth: {
        client_id: "7185575",
        secure_key: "XWvelMAUjB7lcdfALuQ4",
        service_key: "5ce43bf05ce43bf05ce43bf0105c899f5755ce45ce43bf001517695bee76b8274286ea1"
    },
    appSalt: "hb^hbfG^4F5tfgG56tgfr&8y6tgNmj",
}