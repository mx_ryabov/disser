import { Request, Response, NextFunction } from "express";
import { IStartSymptom, IRecommendation, IQuestion } from "../interfaces";
import StartSymptom from "../models/StartSymptom";
import Recommendation from "../models/Recommendation";
import Question from "../models/Question";


export class AdminController {
    updateQuestion = async (req: Request, res: Response, next: NextFunction) => {
        let {_id, title, recommendationLinks} = req.body;

        if (!title || !recommendationLinks) res.status(422).json({});
        
        if (_id) {
            await Question.updateOne({_id}, {title, recommendationLinks});
        } else {
            await Question.create({title, recommendationLinks});
        }

        const resData:IQuestion[] = await (await Question.find().populate({
            path: 'recommendationLinks.recommendation',
            populate: 'symptoms'
        })).reverse();
        res.json(resData)
    }

    getQuestions = async (req: Request, res: Response, next: NextFunction) => {
        const resData:IQuestion[] = await (await Question.find().populate({
            path: 'recommendationLinks.recommendation',
        })).reverse();
        res.json(resData)
    }

    removeQuestion = async (req: Request, res: Response, next: NextFunction) => {
        let {_id} = req.body;

        if (!_id) res.status(422).json({});

        await Question.remove({_id});

        const resData:IQuestion[] = await (await Question.find().populate({
            path: 'recommendationLinks.recommendation',
            populate: 'symptoms'
        })).reverse();
        res.json(resData)
    }

    updateStartSymptom = async (req: Request, res: Response, next: NextFunction) => {
        let {_id, title, type} = req.body;        

        if (!title || !type) res.status(422).json({});

        if (_id) {
            await StartSymptom.updateOne({_id}, {title, type});
        } else {
            await StartSymptom.create({title, type});
        }

        const resData:IStartSymptom[] = await (await StartSymptom.find()).reverse();
        res.json(resData)
    }

    removeStartSymptom = async (req: Request, res: Response, next: NextFunction) => {
        let {_id} = req.body;

        if (!_id) res.status(422).json({});

        await StartSymptom.remove({_id});

        const resData:IStartSymptom[] = await (await StartSymptom.find()).reverse();
        res.json(resData)
    }

    getStartSymptoms = async (req: Request, res: Response, next: NextFunction) => {
        const resData:IStartSymptom[] = await (await StartSymptom.find()).reverse();
        res.json(resData)
    }

    updateRecomendation = async (req: Request, res: Response, next: NextFunction) => {
        let {_id, title, ages, genders, symptoms, plan} = req.body;

        if (!title || !ages || !genders || !symptoms || !plan) res.status(422).json({});
        
        if (_id) {
            await Recommendation.updateOne({_id}, {title, ages, genders, symptoms, plan});
        } else {
            await Recommendation.create({title, ages, genders, symptoms, plan});
        }

        const resData:IRecommendation[] = await (await Recommendation.find()).reverse();
        res.json(resData)
    }

    getRecomendations = async (req: Request, res: Response, next: NextFunction) => {
        const resData:IRecommendation[] = await (await Recommendation.find()).reverse();
        res.json(resData)
    }

    removeRecommendation = async (req: Request, res: Response, next: NextFunction) => {
        let {_id} = req.body;

        if (!_id) res.status(422).json({});

        await Recommendation.remove({_id});

        const resData:IRecommendation[] = await (await Recommendation.find()).reverse();
        res.json(resData)
    }
}