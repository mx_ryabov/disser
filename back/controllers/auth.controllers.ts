import { NextFunction, Request, Response } from "express";
import request from "request";
import passport from "passport";
import "../passportHandler";
import { IBaseUser } from "../interfaces";
import User from "../models/User";
import config from "../config";
import * as jwt from "jsonwebtoken";
import BlackList from "../models/BlackList";
import { Socket } from "socket.io";

export class AuthController {
    public async oauth(req:Request, res:Response, next:NextFunction) {
        request.get(`https://oauth.vk.com/access_token?client_id=${config.oauth.client_id}&client_secret=${config.oauth.secure_key}&code=${req.body.code}`, function (error, response, body) {
            const {access_token, user_id} = JSON.parse(body);
                        
            request.get(`https://api.vk.com/method/account.getProfileInfo?v=5.52&access_token=${access_token}`, async function (error_u, response_u, body_u) {
                const vk_user = JSON.parse(body_u).response;
                
                var user:IBaseUser|null = await User.findOne({vkId: user_id});

                var token:string;
                if (!user) {                    
                    user = new User({
                        login: vk_user.screen_name,
                        firstName: vk_user.first_name,
                        secondName: vk_user.last_name,
                        vkId: user_id
                    });
                    await user.save();
                }
                token = jwt.sign({ login: user.login }, config.session.jwtSecret);
                res.json({token, user});
            });
        });
    }

    public async auth(req:Request, res:Response, next:NextFunction) {
        
        passport.authenticate("local", {session: false}, function (err, user, info) {
            if (err) return next(err);
            if (!user) {
                return res.status(401).json({ status: "error", code: "unauthorized" });
            } else {
                console.log(user);
                
                const token = jwt.sign({ 
                    login: user.login, 
                    firstName: user.firstName, 
                    secondName: user.secondName,
                    _id: user._id
                }, config.session.jwtSecret);

                res.status(200).send({ token: token, user: user });
            }
        })(req, res, next);
    }
    
    public authenticateJWT(req: Request, res:Response, next:NextFunction) {
        passport.authenticate("jwt", async function(err, user:IBaseUser, info) {
            
            if (err) {
                console.log('Controller authenticateJWT error: ', err);
                return res.status(401).json({ status: "error", code: "unauthorized" });
            }
            if (!req.headers.authorization) {
                res.status(401).json({ status: "error", code: "unauthorized" });
            } else {
                const token:string = req.headers.authorization.split(" ")[1];
                const isInsideBL = await BlackList.findOne({token});
                if (isInsideBL) {
                    res.status(401).json({ status: "error", code: "unauthorized" });
                } else {
                    if (!user) {
                        return res.status(401).json({ status: "error", code: "unauthorized" });
                    } else {
                        req.me = user;
                        return next();
                    }
                }
            }
            
        })(req, res, next);
    }

    public logout(req:Request, res:Response, next:NextFunction) {        
        BlackList.create({token: req.body.token});
        res.json({})
    }
        
}