import { IStartSymptom } from "../admin/types";

interface IQuestionPoint {
    min: number;
    max: number;
}

export interface IPatientRecommendation {
    text: string;
    doctor: string | null;
    analysis: string | null;
    index: number;
    points: number;
    questionPoints: IQuestionPoint[];
}

export interface IStartAnswer {
    title: string;
    code: string;
}

export interface IQuestion {
    _id?: string;
    title: string;
    answer: IStartAnswer | null;
    answerToServer?: IAnswerToServer;
    type: "gender" | "age" | "symptom" | "other";
    relatedRecs: IPatientRecommendation[];
}

export interface IAnswerToServer {
    q: string;
    ans: number;
}

export interface IPlan {
    name:string;
    planType:string;
}

export interface ISortedRecs {
    title: string;
    prob: number;
    plan: IPlan[];
}


export interface ITest {
    id: string;
    title: string;
    date: string;
    time: string;
    gender?: string;
    age?: string;
    symptom?: string;
    complete: boolean;
    currentQuestionInd: number;
    currentQuestion?: IQuestion;
    completedQuestions: IQuestion[];
    recomendations: ISortedRecs[];
    progress: number;
}

export interface PatientState {
    fetching: boolean;
    tests: ITest[];
    start_symptoms: IStartSymptom[];
    currentQuestion: IQuestion;
    currentRecommendations: ISortedRecs[];
    currentTest: number | null;
    error?: number;
}


export const START_FETCH = "START_FETCH";
export const FETCH_ERROR = "FETCH_ERROR";

export const CREATE_TEST = "CREATE_TEST";
export const UPDATE_TEST = "UPDATE_TEST";
export const GET_START_SYMPTOMS = "GET_START_SYMPTOMS";
export const GET_NEXT_QUESTION = "GET_NEXT_QUESTION";
export const GET_RECOMMENDATIONS = "GET_RECOMMENDATIONS";
export const SET_SAVED_TESTS = "SET_SAVED_TESTS";
export const REMOVE_TEST = "REMOVE_TEST";


export interface StartFetchAction {
    type: typeof START_FETCH;
}

interface FetchActionError {
    type: typeof FETCH_ERROR;
    payload: number;
}

export interface CreateTest {
    type: typeof CREATE_TEST;
    payload: ITest;
}
export interface UpdateTest {
    type: typeof UPDATE_TEST;
    payload: {
        test: ITest,
        ind: number
    }
}

export interface GetNextQuestion {
    type: typeof GET_NEXT_QUESTION;
    payload: {
        q:IQuestion, 
        test_id:string, 
        recommendations: ISortedRecs[],
        currentAnswer?: IStartAnswer,
        probAnswer?: IAnswerToServer
    };
}

export interface GetRecommendations {
    type: typeof GET_RECOMMENDATIONS;
    payload: ISortedRecs[];
}

export interface GetStartSymptoms {
    type: typeof GET_START_SYMPTOMS;
    payload: IStartSymptom[];
}

export interface SetSavedTests {
    type: typeof SET_SAVED_TESTS;
    payload: ITest[];
}

export interface RemoveTest {
    type: typeof REMOVE_TEST;
    payload: string;
}



export type MainActionTypes = StartFetchAction 
                              | FetchActionError
                              | GetNextQuestion
                              | GetStartSymptoms
                              | GetRecommendations
                              | CreateTest
                              | UpdateTest
                              | SetSavedTests
                              | RemoveTest;
