import {
    START_FETCH, FETCH_ERROR, MainActionTypes, StartFetchAction, 
    GET_NEXT_QUESTION, GET_START_SYMPTOMS, IAnswerToServer, 
    CREATE_TEST, ITest, UPDATE_TEST, IStartAnswer, SET_SAVED_TESTS, REMOVE_TEST
} from './types';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { getRequest } from '../../helpers';
import { IStartSymptom } from '../admin/types';

type ThunkAct = ThunkAction<Promise<void>, {}, {}, AnyAction>;
type ThunkDisp = ThunkDispatch<{}, {}, AnyAction>;


export const getNextQuestion = (
    gender: string, age:string, sym_id:string, 
    test_id:string, currentAnswer?: IStartAnswer, answers?:IAnswerToServer[]
):ThunkAct => async (dispatch:ThunkDisp) => {

    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);

    try {
        let answersString:string = JSON.stringify(answers || []);
        const res:any = await getRequest(
                                "/api/patient/next_question/get", 
                                {gender, age, sym_id, answers: answersString}
                              );
        
        const success_q:MainActionTypes = {
            type: GET_NEXT_QUESTION, 
            payload: {
                q: res.question, 
                test_id, 
                recommendations: res.recommendations,
                currentAnswer,
                probAnswer: answers?.pop()
            }
        };
        dispatch(success_q);
    } catch (e) {
        console.log(e);
        
        const error:MainActionTypes = {type: FETCH_ERROR, payload: e.response.status };
        dispatch(error);
    }
}

export const getStartSymptoms = ():ThunkAct => async (dispatch:ThunkDisp) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);     

    try {
        const res:IStartSymptom[] = await getRequest("/api/patient/start_symptoms/get", {});
        
        const success:MainActionTypes = {type: GET_START_SYMPTOMS, payload: res};
        dispatch(success);
    } catch (e) {
        console.log(e);
        
        const error:MainActionTypes = {type: FETCH_ERROR, payload: e.response.status };
        dispatch(error);
    }
}

export const createTest = (test:ITest):ThunkAct => async (dispatch:ThunkDisp) => {
    const success:MainActionTypes = {type: CREATE_TEST, payload: test};
        
    dispatch(success);
}

export const updateTest = (test:ITest, ind:number):ThunkAct => async (dispatch:ThunkDisp) => {
    const success:MainActionTypes = {type: UPDATE_TEST, payload: {test, ind}};
        
    dispatch(success);
}

export const setSavedTests = (tests:ITest[]):ThunkAct => async (dispatch:ThunkDisp) => {
    const success:MainActionTypes = {type: SET_SAVED_TESTS, payload: tests};
        
    dispatch(success);
}

export const removeTest = (id:string):ThunkAct => async (dispatch:ThunkDisp) => {
    const success:MainActionTypes = {type: REMOVE_TEST, payload: id};
        
    dispatch(success);
}