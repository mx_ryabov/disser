import { 
    START_FETCH, FETCH_ERROR, PatientState, 
    MainActionTypes,
    GET_NEXT_QUESTION,
    GET_START_SYMPTOMS,
    GET_RECOMMENDATIONS,
    CREATE_TEST,
    UPDATE_TEST,
    ITest,
    SET_SAVED_TESTS,
    REMOVE_TEST
} from './types';


const initialState: PatientState = {
    fetching: false,
    error: undefined,
    start_symptoms: [],
    currentQuestion: {title: "", answer: null, type: "other", relatedRecs: []},
    currentRecommendations: [],
    currentTest: null,
    tests: []
};

export function patientReducer(
    state = initialState,
    action: MainActionTypes
): PatientState {
    switch (action.type) {

        case REMOVE_TEST:
            return {
                ...state,
                tests: state.tests.filter((test:ITest) => test.id !== action.payload)
            }

        case SET_SAVED_TESTS:
            return {
                ...state,
                tests: action.payload
            }

        case GET_NEXT_QUESTION:
            let progress:number = ([...action.payload.recommendations].shift()?.prob || 0.3) * 100;
            progress = progress < 30 ? 30 : progress;
            progress = Math.trunc(progress);
            return {
                ...state,
                fetching: false,
                tests: state.tests.map((test:ITest) => {
                    return test.id === action.payload.test_id ?
                            {
                                ...test, 
                                recomendations: action.payload.recommendations,
                                progress,
                                completedQuestions: test.currentQuestion?._id ? 
                                        [
                                            ...test.completedQuestions, 
                                            {
                                                ...test.currentQuestion, 
                                                answer: action.payload.currentAnswer || null,
                                                answerToServer: action.payload.probAnswer
                                            }
                                        ] : 
                                        test.completedQuestions,

                                currentQuestion: {...action.payload.q, answer: null, type: "other"}
                            } :
                            test;
                })
            }

        case GET_RECOMMENDATIONS:
            return {
                ...state,
                fetching: false,
                currentRecommendations: action.payload
            }

        case GET_START_SYMPTOMS:
            return {
                ...state,
                fetching: false,
                start_symptoms: action.payload
            }

        case CREATE_TEST:
            return {
                ...state,
                fetching: false,
                tests: [action.payload, ...state.tests]
            }

        case UPDATE_TEST:
            return {
                ...state,
                tests: state.tests.map((test:ITest, ind:number) => {
                    return ind === action.payload.ind ? action.payload.test : test
                })
            }

        case START_FETCH:
            return {
                ...state,
                fetching: true,
                error: undefined
            };

        case FETCH_ERROR:
            return {
                ...state,
                fetching: false,
                error: action.payload
            };


        default:
            return state;

    }
}