import {
    AdminActionTypes,
    AdminState,
    AUTH_SUCCESS,
    GET_ME_SUCCESS,
    AUTH,
    AUTH_ERROR,
    GET_SYMPTOMS,
    UPDATE_SYMPTOM,
    REMOVE_SYMPTOM,
    START_FETCH,
    FETCH_ERROR,
    LOGOUT,
    GET_RECOMMENDATIONS,
    UPDATE_RECOMMENDATION,
    REMOVE_RECOMMENDATION,
    REMOVE_QUESTION,
    GET_QUESTIONS,
    UPDATE_QUESTION,
  } from "./types";
  
  const initialState: AdminState = {
    fetching: true,
    me: { login: "", firstName: "", _id: "", secondName: "" },
    status: null,
    symptoms: [],
    recommendations: [],
    questions: [],
    error: undefined
  };
  
  export function adminReducer( 
      state = initialState, 
      action: AdminActionTypes
  ): AdminState {

      switch (action.type) {

        case UPDATE_QUESTION:
            return {
                ...state,
                fetching: false,
                questions: action.payload
            }

        case GET_QUESTIONS:
            return {
                ...state,
                fetching: false,
                questions: action.payload
            }

        case REMOVE_QUESTION:
            return {
                ...state,
                fetching: false,
                questions: action.payload
            }

        case GET_RECOMMENDATIONS:
            return {
                ...state,
                fetching: false,
                recommendations: action.payload
            }

        case UPDATE_RECOMMENDATION:
            return {
                ...state,
                fetching: false,
                recommendations: action.payload
            }

        case REMOVE_RECOMMENDATION:
            return {
                ...state,
                fetching: false,
                recommendations: action.payload
            }

        case GET_SYMPTOMS:
            return {
                ...state,
                symptoms: action.payload,
                fetching: false
            }

        case UPDATE_SYMPTOM:
            return {
                ...state,
                symptoms: action.payload,
                fetching: false
            }

        case REMOVE_SYMPTOM:
            return {
                ...state,
                symptoms: action.payload,
                fetching: false
            } 

        case GET_ME_SUCCESS:
            return {
                ...state,
                me: action.payload,
                fetching: false,
                status: "storage"
            }

        case AUTH:
            return {
                ...state,
                fetching: true,
                status: "signin",
                error: undefined
            }

        case AUTH_SUCCESS:
            return {
                ...state,
                me: action.payload,
                fetching: false,
                status: "authenticated"
            };

        case AUTH_ERROR:
            return {
                ...state,
                fetching: false,
                status: "signin",
                error: action.payload
            }

        case LOGOUT:
            return {
                ...state,
                status: "signin",
                fetching: false
            }

        case START_FETCH:
            return {
                ...state,
                fetching: true,
                error: undefined
            };

        case FETCH_ERROR:
            return {
                ...state,
                fetching: false,
                error: action.payload
            };

        default:
            return state;
      }
  }
