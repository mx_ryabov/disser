import {
    START_FETCH, FETCH_ERROR, AdminActionTypes, StartFetchAction, 
    AUTH_SUCCESS, AUTH_ERROR, GET_SYMPTOMS, IStartSymptom, SYMPTOM_TYPE, 
    UPDATE_SYMPTOM,
    REMOVE_SYMPTOM,
    IAdmin,
    GET_ME_SUCCESS,
    LOGOUT,
    IRecommendation,
    UPDATE_RECOMMENDATION,
    GET_RECOMMENDATIONS,
    REMOVE_RECOMMENDATION,
    UPDATE_QUESTION,
    REMOVE_QUESTION,
    GET_QUESTIONS,
    IRecommendationLink
} from './types';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { postRequest, getRequest } from '../../helpers';
import { IQuestion } from '../admin/types';

type ThunkAct = ThunkAction<Promise<void>, {}, {}, AnyAction>;
type ThunkDisp = ThunkDispatch<{}, {}, AnyAction>;


export const getQuestions = ():ThunkAct => async (dispatch:ThunkDisp) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);     

    try {
        const res:IQuestion[] = await getRequest("/api/admin/question/get", {});
        
        const success:AdminActionTypes = {type: GET_QUESTIONS, payload: res};
        dispatch(success);
    } catch (e) {
        console.log(e);
        
        const error:AdminActionTypes = {type: FETCH_ERROR, payload: e.response.status };
        dispatch(error);
    }
}

export const removeQuestion = (_id:string):ThunkAct => async (dispatch:ThunkDisp) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);     

    try {
        const res:IQuestion[] = await postRequest("/api/admin/question/remove", {_id});
        
        const success:AdminActionTypes = {type: REMOVE_QUESTION, payload: res};
        dispatch(success);
    } catch (e) {
        console.log(e);
        
        const error:AdminActionTypes = {type: FETCH_ERROR, payload: e.response.status };
        dispatch(error);
    }
}

export const updateQuestion = (q:IQuestion):ThunkAct => async (dispatch:ThunkDisp) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);     

    try {
        const reqData:any = {
            ...q,
            recommendationLinks: q.recommendationLinks.map((rl:IRecommendationLink) => {
                return {...rl, recommendation: rl.recommendation._id}
            }),
        }
        const res:IQuestion[] = await postRequest("/api/admin/question/update", reqData);
        
        const success:AdminActionTypes = {type: UPDATE_QUESTION, payload: res};
        dispatch(success);
    } catch (e) {
        console.log(e);
        
        const error:AdminActionTypes = {type: FETCH_ERROR, payload: e.response.status };
        dispatch(error);
    }
}


export const getRecommendations = ():ThunkAct => async (dispatch:ThunkDisp) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);     

    try {
        const res:IRecommendation[] = await getRequest("/api/admin/recommendation/get", {});
        
        const success:AdminActionTypes = {type: GET_RECOMMENDATIONS, payload: res};
        dispatch(success);
    } catch (e) {
        console.log(e);
        
        const error:AdminActionTypes = {type: FETCH_ERROR, payload: e.response.status };
        dispatch(error);
    }
}

export const removeRecommendation = (_id:string):ThunkAct => async (dispatch:ThunkDisp) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);     

    try {
        const res:IRecommendation[] = await postRequest("/api/admin/recommendation/remove", {_id});
        
        const success:AdminActionTypes = {type: REMOVE_RECOMMENDATION, payload: res};
        dispatch(success);
    } catch (e) {
        console.log(e);
        
        const error:AdminActionTypes = {type: FETCH_ERROR, payload: e.response.status };
        dispatch(error);
    }
}

export const updateRecommendation = (rec:IRecommendation):ThunkAct => async (dispatch:ThunkDisp) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);     

    try {
        const res:IRecommendation[] = await postRequest("/api/admin/recommendation/update", {...rec});
        
        const success:AdminActionTypes = {type: UPDATE_RECOMMENDATION, payload: res};
        dispatch(success);
    } catch (e) {
        console.log(e);
        
        const error:AdminActionTypes = {type: FETCH_ERROR, payload: e.response.status };
        dispatch(error);
    }
}

export const getMe = ():ThunkAct => async (dispatch:ThunkDisp) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);    

    try {
        const res:IAdmin = (await getRequest("/api/user/get", {})).me;
        
        const success:AdminActionTypes = {type: GET_ME_SUCCESS, payload: res};
        dispatch(success);
    } catch (e) {        
        const error:AdminActionTypes = {type: FETCH_ERROR, payload: e.response.status };
        dispatch(error);
    }
}

export const logIn = (login:string, password:string):ThunkAct => async (dispatch:ThunkDisp) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching); 

    try {
        const res:any = await postRequest("/api/user/login", {login, password});
        sessionStorage.setItem("token", res.token);
        
        const success:AdminActionTypes = {type: AUTH_SUCCESS, payload: res.user};
        dispatch(success);
    } catch (e) {
        const error:AdminActionTypes = {type: AUTH_ERROR, payload: e.response.status };
        dispatch(error);
    }
}

export const removeStartSymptom = ( _id: string):ThunkAct => async (dispatch:ThunkDisp) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);    

    try {
        const res:IStartSymptom[] = await postRequest("/api/admin/symptom/remove", {_id});
        
        const success:AdminActionTypes = {type: REMOVE_SYMPTOM, payload: res};
        dispatch(success);
    } catch (e) {
        const error:AdminActionTypes = {type: FETCH_ERROR, payload: e.response.status };
        dispatch(error);
    }
}

export const updateStartSymptom = (title:string, type: SYMPTOM_TYPE, _id?: string):ThunkAct => async (dispatch:ThunkDisp) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching); 
    console.log(title, type, _id);
    

    try {
        const res:IStartSymptom[] = await postRequest("/api/admin/symptom/update", {title, type, _id});
        
        const success:AdminActionTypes = {type: UPDATE_SYMPTOM, payload: res};
        dispatch(success);
    } catch (e) {
        const error:AdminActionTypes = {type: FETCH_ERROR, payload: e.response.status };
        dispatch(error);
    }
}

export const getStartSymptoms = ():ThunkAct => async (dispatch:ThunkDisp) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching); 

    try {
        const res:IStartSymptom[] = await getRequest("/api/admin/symptom/get", {});
        
        const success:AdminActionTypes = {type: GET_SYMPTOMS, payload: res};
        dispatch(success);
    } catch (e) {
        const error:AdminActionTypes = {type: FETCH_ERROR, payload: e.response.status };
        dispatch(error);
    }
}

export const logout = ():ThunkAct => async (dispatch:ThunkDisp) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching); 

    try {
        postRequest("/api/user/logout", {});
        
        const success:AdminActionTypes = {type: LOGOUT};
        dispatch(success);
    } catch (e) {
        const error:AdminActionTypes = {type: FETCH_ERROR, payload: e.response.status };
        dispatch(error);
    }
}
/*
const fetchWrapper = (dispatch:ThunkDisp, mainAction: () => Promise<void>) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);   
    
    try {
        mainAction();
    } catch(e) {
        const error:AdminActionTypes = {type: FETCH_ERROR, payload: e };
        dispatch(error);
    }
}*/