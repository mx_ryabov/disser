import React, { Component } from 'react';
import Table, { IBasicBodyItem, ITable } from '../../components/admin/Table';
import AdminSlideFormContainer from '../../components/admin/AdminSlideFormContainer';
import QuestionForm from '../../components/admin/QuestionForm';
import { AppState } from '../../store';
import { ThunkDispatch } from 'redux-thunk';
import { getQuestions, updateQuestion, removeQuestion, getRecommendations, getStartSymptoms } from '../../store/admin/actions';
import { IQuestion, IRecommendationLink } from '../../store/admin/types';
import { connect } from 'react-redux';
import { AdminState } from '../../store/admin/types';

interface props {
    getQuestions: () => Promise<void>;
    removeQuestion: (_id:string) => Promise<void>;
    updateQuestion: (q:IQuestion) => Promise<void>;
    getRecommendations: () => Promise<void>;
    getStartSymptoms: () => Promise<void>;
    admin: AdminState;
}

interface state {
    table: ITable<IBasicBodyItem>;
}

class QuestionPage extends Component<props, state> {
    state = {
        table: {
            header: [
                {title: "Наименование"},
                {title: "Рекомендация"}
            ],
            body: []
        }
    }

    adminSlideForm:AdminSlideFormContainer | null;
    questionForm: QuestionForm | null;

    componentDidMount() {
        this.props.getStartSymptoms();
        this.props.getRecommendations();
        this.props.getQuestions();
    }

    componentDidUpdate(oldProps:props) {
        if (oldProps.admin.questions !== this.props.admin.questions)  
            this.setState({
                table: {
                    ...this.state.table,
                    body: this._transformToTableView(this.props.admin.questions)
                }
            })
    }

    _transformToTableView(questions:IQuestion[]):IBasicBodyItem[] {
        return questions.map((q:IQuestion, ind: number) => {
            return {
                title: q.title,
                _id: q._id || "",
                otherCells: [
                    {
                        title: q.recommendationLinks.map((rl:IRecommendationLink) => {
                            return rl.recommendation.title
                        }).join(", "), 
                        type: "simple", 
                        tooltipText: ""
                    },
                ]
            }
        })
    }

    openForm = (_id?:string) => {
        this.adminSlideForm?.onClickRow();
        this.questionForm?.onActive(
            this.props.admin.questions.filter((q:IQuestion) => q._id === _id)[0]    
        )
    }

    updateAction = (q:IQuestion) => {
        this.props.updateQuestion(q);
        this.adminSlideForm?.closeForm();
    }

    removeAction = (_id:string) => {
        this.props.removeQuestion(_id);
        this.adminSlideForm?.closeForm();
    }

    _renderContent = () => {
        if (this.props.admin.questions.length === 0) {
            return (
                <div className="empty-state-container">
                    <h1>Не создано ни одного вопроса</h1>
                    <button 
                        className="btn" 
                        onClick={() => {this.openForm()}}
                        data-type="filled"
                    >создать</button>
                </div>
            )
        } else {
            return (
                <>
                <button 
                    className="btn" 
                    onClick={() => {this.openForm()}}
                    data-type="filled"
                >создать вопрос</button>
                <div className="table-contaiter" id="style-1">
                    <Table 
                        data={this.state.table} 
                        onClickRow={this.openForm}
                    />
                </div>
                </>
            )
        }
    }

    render() {
        return (
            <div className="content">
                <AdminSlideFormContainer ref={node => this.adminSlideForm = node}>
                    <QuestionForm 
                        recommendations={this.props.admin.recommendations}
                        updateAction={this.updateAction}
                        removeAction={this.removeAction}
                        symptoms={this.props.admin.symptoms}
                        ref={node => this.questionForm = node}
                    />
                </AdminSlideFormContainer>
                {this._renderContent()}
            </div>
        )
    }
}


const mapStateToProps = (state: AppState) => ({
    admin: state.admin
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
    getQuestions: async () => {
        dispatch(getQuestions());
    },
    updateQuestion: async (q:IQuestion) => {
        dispatch(updateQuestion(q));
    },
    removeQuestion: async (_id:string) => {
        dispatch(removeQuestion( _id));
    },
    getRecommendations: async () => {
        dispatch(getRecommendations());
    },
    getStartSymptoms: async () => {
        dispatch(getStartSymptoms());
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(QuestionPage);


