import React, { Component } from 'react'
import AdminSlideFormContainer from '../../components/admin/AdminSlideFormContainer'
import Table, { IBasicBodyItem, ITable } from '../../components/admin/Table';

export default class ReviewPage extends Component {
    adminSlideForm:AdminSlideFormContainer | null;

    onClickRow = (_id:string) => {
        this.adminSlideForm?.onClickRow();
    }
    
    render() {
        return (
            <div className="content">
                <AdminSlideFormContainer ref={node => this.adminSlideForm = node}></AdminSlideFormContainer>
                <Table 
                    data={dataTable} 
                    onClickRow={this.onClickRow}
                />
            </div>
        )
    }
}

const dataTable:ITable<IReviewBodyItem> = {
    header: [
        {title: "Рекомендация"},
        {title: "Статус"}
    ],
    body: [
        {
            title: "Простуда 95%, Грипп 80%", 
            _id: "0", 
            otherCells: [
                {title: "не рассмотрено", type: "status_empty", tooltipText: ""},
            ]
        },
        {
            title: "Вы много курите?", 
            _id: "1", 
            otherCells: [
                {title: "успешно", type: "status_success", tooltipText: "Рак легкого, абсцесс легкого"},
            ]
        },
        {
            title: "Есть ли у вас повышенная температура?", 
            _id: "2", 
            otherCells: [
                {title: "с ошибкой", type: "status_warning", tooltipText: "Грипп, простуда, пневмания"}
            ]
        }
    ]
}

interface IReviewBodyItem extends IBasicBodyItem {

}