import React, { Component } from 'react';
import '../../assets/scss/patient.scss';
import ProgressBar from '../../components/patient/ProgressBar';
import NavigationBar from '../../components/patient/NavigationBar';
import LeftMenuBoard from '../../components/patient/LeftMenuBoard';
import RightInfoBoard from '../../components/patient/RightInfoBoard';
import Test from '../../components/patient/Test';
import { ITest, PatientState, IAnswerToServer, IStartAnswer } from '../../store/patient/types';
import { AppState } from '../../store';
import { ThunkDispatch } from 'redux-thunk';
import { getNextQuestion, getStartSymptoms, createTest, updateTest, setSavedTests, removeTest } from '../../store/patient/actions';
import { connect } from 'react-redux';
import { IStartSymptom } from '../../store/admin/types';

interface props {
    patient: PatientState;
    getNextQuestion: (
        gender: string, age:string, sym_id:string, 
        test_id:string, currentAnswer?: IStartAnswer, 
        answers?:IAnswerToServer[]
        ) => Promise<void>;
    getStartSymptoms: () => Promise<void>;
    setSavedTests: (tests:ITest[]) => void;
    createTest: (test:ITest) => void;
    updateTest: (test:ITest, ind:number) => void;
    removeTest: (id:string) => void;
}

interface state {
    progressProc: number;
    isActiveLeftBoard: boolean;
    isActiveRightBoard: boolean;
    currentTestInd?: number;
    isNew: boolean;
}

class MainPage extends Component<props, state> {
    leftBoard:LeftMenuBoard | null;
    state:state = {
        progressProc: 0,
        isActiveLeftBoard: false,
        isActiveRightBoard: false,
        isNew: false
    }

    componentDidMount() {
        this.props.getStartSymptoms();
        const savedTestsStr:string | null = localStorage.getItem("tests");
        if (savedTestsStr) {
            this.props.setSavedTests(JSON.parse(savedTestsStr));
        }
    }

    componentDidUpdate(prevProps:props) {
        if (this.props.patient.tests.length !== prevProps.patient.tests.length) {
            if (this.state.isNew) {
                this.setState({
                    currentTestInd: 0,
                    isNew: false
                })
            }
        }
        if (this.state.currentTestInd !== undefined) {
            const oldTestVersion:ITest = prevProps.patient.tests[this.state.currentTestInd];
            const newTestVersion:ITest = this.props.patient.tests[this.state.currentTestInd];

            if (oldTestVersion !== newTestVersion) {
                localStorage.setItem("tests", JSON.stringify(this.props.patient.tests));
            }
        }
        if (this.props.patient.tests.length < prevProps.patient.tests.length) {
            localStorage.setItem("tests", JSON.stringify(this.props.patient.tests));
        }
    }

    openBoard(side: "left" | "right") {
        side === "left" ? this.setState({ isActiveLeftBoard: true }) : 
                          this.setState({ isActiveRightBoard: true });
    }
    closeBoard(side: "left" | "right") {
        side === "left" ? this.setState({ isActiveLeftBoard: false }) : 
                          this.setState({ isActiveRightBoard: false });
    }

    onCreateTest = (test:ITest) => {
        this.props.createTest(test);
        this.setState({
            isNew: true
        });
    }

    getSymptomById = (sym_id:string):IStartSymptom => {
        return this.props.patient.start_symptoms.filter(sym => sym._id === sym_id)[0];
    }

    onUpdateTest = (test:ITest) => {
        if (this.state.currentTestInd !== undefined)
            this.props.updateTest(test, this.state.currentTestInd);
    }

    onSelectTest = (ind:number) => {
        this.setState({ currentTestInd: ind });
    }

    finishTest = () => {
        let currentTest:ITest | undefined;
        
        if (this.state.currentTestInd !== undefined) {
            currentTest = this.props.patient.tests[this.state.currentTestInd];
            this.props.updateTest({
                ...currentTest,
                complete: true
            }, this.state.currentTestInd);
        }
    }

    removeTest = () => {
        let currentTest:ITest | undefined;
        if (this.state.currentTestInd !== undefined) {
            currentTest = this.props.patient.tests[this.state.currentTestInd];
            if (currentTest !== undefined)
                this.setState({
                    currentTestInd: undefined
                }, this.props.removeTest.bind(null, currentTest.id))
        }
        
    }


    render() {
        let isOverlayActive:boolean = this.state.isActiveRightBoard || this.state.isActiveLeftBoard;
        let currentTest:ITest | undefined;
        if (this.state.currentTestInd !== undefined) currentTest = this.props.patient.tests[this.state.currentTestInd];
        
        return (
            <div className="patient page">
                <ProgressBar 
                    proc={currentTest?.progress || 0} 
                    openInfo={this.openBoard.bind(this, "right")}
                />
                <LeftMenuBoard 
                    onSelectItem={this.onSelectTest}
                    onCreateTest={this.onCreateTest}
                    isActive={this.state.isActiveLeftBoard} 
                    closeAction={this.closeBoard.bind(this, "left")}
                    tests={this.props.patient.tests}
                    ref={node => this.leftBoard = node}
                />
                <Test 
                    data={currentTest}
                    currentQuestion={this.props.patient.currentQuestion}
                    getSymptomById={this.getSymptomById}
                    updateTest={this.onUpdateTest}
                    startSymptoms={this.props.patient.start_symptoms}
                    getNextQuestion={this.props.getNextQuestion}
                    createNewTest={this.leftBoard?.createTest}
                />
                <RightInfoBoard
                    recommendations={[...currentTest?.recomendations || []].splice(0, 3)}
                    isActive={this.state.isActiveRightBoard} 
                    closeAction={this.closeBoard.bind(this, "right")}
                    progressProc={currentTest?.progress || 0}
                    finishTest={this.finishTest}
                    removeTest={this.removeTest}
                    isFinish={currentTest?.complete || false}
                />
                <NavigationBar 
                    openLeft={this.openBoard.bind(this, "left")}
                    openRight={this.openBoard.bind(this, "right")}
                />
                <div 
                    className={`overlay ${isOverlayActive ? "active" : ""}`}
                    onClick={this.closeBoard.bind(
                        this,
                        this.state.isActiveLeftBoard ? "left" : "right"
                    )}
                ></div>
            </div>
        )
    }
}


const mapStateToProps = (state: AppState) => ({
    patient: state.patient
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
    getNextQuestion: async (
        gender: string, age:string, sym_id:string, 
        test_id:string, currentAnswer?: IStartAnswer, 
        answers?:IAnswerToServer[]
    ) => {        
        dispatch(getNextQuestion(
            gender, age, sym_id, test_id, currentAnswer, answers
        ));
    },
    getStartSymptoms: async () => {
        dispatch(getStartSymptoms());
    },
    createTest: (test:ITest) => {
        dispatch(createTest(test));
    },
    updateTest: (test:ITest, ind:number) => {
        dispatch(updateTest(test, ind));
    },
    setSavedTests: (tests:ITest[]) => {
        dispatch(setSavedTests(tests));
    },
    removeTest: (id:string) => {
        dispatch(removeTest(id));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);