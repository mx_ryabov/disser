import { Component } from 'react';
import ReactDOM from 'react-dom';

interface props {
    className: string;
    onHide?: () => void;
}

export default class ModalContainer extends Component<props, {}> {
    modal:HTMLDivElement = window.document.createElement('div');
    overlay:HTMLDivElement = window.document.createElement('div');
    body:HTMLBodyElement = window.document.getElementsByTagName("body")[0];

    componentDidMount() {
        this.modal.classList.add("modal", this.props.className);
        this.overlay.classList.add("overlay");
        this.body.appendChild(this.modal)
        this.body.appendChild(this.overlay);

        this.overlay.addEventListener('click', this.hideModal.bind(this));
    }

    hideModal = () => {
        //this.body.style.overflow = "initial";
        this.modal.classList.remove("active");
        this.overlay.classList.remove("active");
        
        setTimeout(() => {
            //this.overlay.style.display = "none";
            this.modal.style.display = "none";
        }, 300);
        if (this.props.onHide) this.props.onHide();
    }

    showModal = () => {
        //this.body.style.overflow = "hidden";
        //this.overlay.style.display = "block";
        this.modal.style.display = "block";

        setTimeout(() => {
            this.overlay.classList.add("active");
            this.modal.classList.add("active");
        }, 10);
    }

    render() {
        return ReactDOM.createPortal(
            this.props.children,
            this.modal
        )
    }
}
