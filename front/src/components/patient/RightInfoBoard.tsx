import React, { Component } from 'react';
import { ISortedRecs, IPlan } from '../../store/patient/types';

interface props {
    isActive: boolean;
    progressProc: number;
    recommendations: ISortedRecs[];
    isFinish: boolean;
    closeAction: () => void;
    finishTest: () => void;
    removeTest: () => void;
}

export default class RightInfoBoard extends Component<props, {}> {
    _renderRecommendations = () => {        
        if (this.props.recommendations.length > 0)            
            return (
                <div className="card-container" id="style-1">
                    <h2>Рекомендации к дальнейшим действиям</h2>
                    {this.props.recommendations.map((rec:ISortedRecs, ind:number) => {
                        return (
                            <div key={ind} className="recommendation">
                                <p>Вероятность: {rec.prob*100}%</p>
                                {
                                    rec.plan.map((planItem:IPlan, planInd:number) => {
                                        return planItem.planType === "doctor" ? 
                                                (<p key={planInd}>{planInd + 1}. Обратитесь к <b>{planItem.name}</b></p>) :
                                                (<p key={planInd}>{planInd + 1}. Пройдите анализ <b>{planItem.name}</b></p>)
                                    })
                                }
                            </div>
                        )
                    })}
                </div>
            )
    }

    onFinishTest = () => {
        this.props.finishTest();
        this.props.closeAction();
    }

    onRemoveTest = () => {
        this.props.removeTest();
        this.props.closeAction();
    }

    render() {
        return (
            <div className={`board right-info ${this.props.isActive ? "active" : ""}`}>
                <div className="card-container" id="style-1">
                    <h2>Прогресс тестирования</h2>
                    <p>Чтобы рекомендация для Вас была более верной и точной, 
                        отвечайте на вопросы, пока шкала не дойдёт до 100%</p>
                    <div className="progress__bar">
                        <div 
                            className="progress__bar-fill" 
                            style={{width: `${this.props.progressProc}%`}}
                        >
                            {this.props.progressProc}%
                        </div>
                    </div>
                    <div className="actions">
                    {
                        this.props.progressProc > 30 && !this.props.isFinish ? 
                            (<>
                                <button onClick={this.onFinishTest} className="btn" data-type="ghost">завершить</button>
                            </>) : ""
                    }
                    {
                        this.props.progressProc > 30 ? (<button onClick={this.onRemoveTest} className="btn" data-type="ghost">удалить</button>) : ""
                    }
                    </div>
                </div>
                {this._renderRecommendations()}
                <button 
                    className="close"
                    onClick={this.props.closeAction}
                ></button>
            </div>
        )
    }
}
