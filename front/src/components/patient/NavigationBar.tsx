import React, { Component } from 'react';

interface props {
    openLeft: () => void;
    openRight: () => void;
}

interface state {

}

export default class NavigationBar extends Component<props, state> {
    render() {
        return (
            <nav className="side-nav">
                <div 
                    className="side-nav__left-opener"
                    onClick={this.props.openLeft}
                ></div>
                <div 
                    className="side-nav__right-opener"
                    onClick={this.props.openRight}
                ></div>
            </nav>
        )
    }
}
