import React, { Component } from 'react';

interface AbstractAdminState {
    isFormOpened: boolean;
}


export default class AdminSlideFormContainer extends Component<{}, AbstractAdminState> {
    page:HTMLElement;
    formWidth:number;
    overlay:HTMLDivElement = window.document.createElement('div');
    body:HTMLBodyElement = window.document.getElementsByTagName("body")[0];
    thisRef:React.RefObject<HTMLDivElement> = React.createRef();

    state = {
        isFormOpened: false
    }

    componentDidMount() {
        this.page = document.getElementsByClassName("admin")[0] as HTMLElement;
        this.formWidth = (document.getElementsByClassName("admin-form")[0] as HTMLElement).offsetWidth;
        this.overlay.classList.add("overlay", "overlay-admin-form");
        this.body.appendChild(this.overlay);
        this.overlay.addEventListener('click', this.closeForm);
    }

    openForm = () => {
        this.setState({
            isFormOpened: true
        }, () => {
            this.overlay.classList.add("active");
            this.page.style.transform = this.overlay.style.transform = `translate(${-this.formWidth}px, 0)`;
            this.thisRef.current?.scrollTo(0, 0);  
        });
    }

    closeForm = () => {
        this.setState({
            isFormOpened: false
        }, () => {
            this.overlay.classList.remove("active");
            this.page.style.transform = this.overlay.style.transform = `translate(0px, 0)`;
        });
    }

    onClickRow = () => {
        this.openForm();
    }

    render() {
        return (
            <div className="admin-form" ref={this.thisRef}>
                {this.props.children}
            </div>
        )
    }
}
