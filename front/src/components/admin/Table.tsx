import React, { Component } from 'react';

interface props<IBodyItem extends IBasicBodyItem> {
    data: ITable<IBodyItem>;
    onClickRow: (_id:string) => void;
}

export default class Table<IBodyItem extends IBasicBodyItem> extends Component<props<IBodyItem>, {}> {
    _renderHeader = () => {
        return this.props.data.header.map((item:IHeaderItem, ind:number) => {
            return (
                <th key={ind} className="light">{item.title}</th>
            )
        })
    }

    _renderBody = () => {
        return this.props.data.body.map((item:IBodyItem, ind:number) => {
            return (
                <tr 
                    key={ind}
                    onClick={this.props.onClickRow.bind(null, item._id)}
                >
                    <td>
                        <div className="circle">{item.title[0]}</div>
                    </td>
                    <td><h2 className="regular">{item.title}</h2></td>
                    {
                        item.otherCells.map((cell:IBodyCell, indCell:number) => {
                            return (
                                <td 
                                    key={indCell} 
                                    cell-type={cell.type}
                                    tooltip-text={cell.tooltipText}
                                >
                                    {cell.title}
                                </td>
                            )
                        })
                    }
                </tr>
            )
        })
    }

    render() {
        return (
            <table className="admin-table">
                <thead>
                    <tr>
                        <th></th>
                        {this._renderHeader()}
                    </tr>
                </thead>
                <tbody>
                    {this._renderBody()}
                </tbody>
            </table>
        )
    }
}

export interface ITable<T extends IBasicBodyItem> {
    header: IHeaderItem[];
    body: T[];
}

interface IHeaderItem {
    title: string;
}

export interface IBasicBodyItem {
    _id: string;
    title: string;
    otherCells: IBodyCell[];
}

interface IBodyCell {
    title: string;
    type: "simple" | "status_success" | "status_warning" | "status_empty";
    tooltipText: string;
}