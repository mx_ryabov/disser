import React, { Component, ComponentState } from 'react';
import { IQuestion, IRecommendation, IRecommendationLink, IStartSymptom } from '../../store/admin/types';

interface props {
    updateAction: (q:IQuestion) => void;
    removeAction: (_id:string) => void;
    recommendations: IRecommendation[];
    symptoms: IStartSymptom[];
}

interface state {
    _id?:string;
    title: string;
    searchRecItem: string;
    errors: {
        title: string;
        recommendationLinks: string;
    };
    foundRecList: IRecommendation[];
    recommendationLinks: IRecommendationLink[];
    isSearchFieldFocused: boolean;
    isFormValid: boolean;
}

const initialState:state = {
    _id: undefined,
    title: "",
    searchRecItem: "",
    errors: {
        title: "",
        recommendationLinks: ""
    },
    foundRecList: [],
    recommendationLinks: [],
    isSearchFieldFocused: false,
    isFormValid: false
}

export default class QuestionForm extends Component<props, state> {
    state = initialState;

    onHandleChange = (e:React.ChangeEvent<HTMLInputElement>) => {
        let {name, value} = e.target;
        if (name === "searchRecItem") this.searchRecItems(value);
        this.setState(
            {[name] : value} as ComponentState,
            () => this.validatingForm(name)
        );
    }

    setError = (field: "title" | "recommendationLinks", mess: string) => {
        this.setState({
            errors: {
                ...this.state.errors,
                [field]: mess
            }
        }, () => {
            let lenErrors:number = 0;
            for (let field in this.state.errors) {
                lenErrors += Reflect.get(this.state.errors, field).length;            
            }
            this.setState({isFormValid: lenErrors === 0})
        })
    }

    validatingForm = async (field: string) => {
        switch(field) {
            case "title":
                await this.setError("title", 
                    this.state.title.length === 0 ? "Поле обязательное для ввода" : ""
                );
                break;
            
            case "recommendationLinks":
                await this.setError("recommendationLinks", 
                    this.state.recommendationLinks.length === 0 ? "*Добавьте хотя бы один вариант" : ""
                );
                break;

            default:
                break;
        }
    }

    onSelectRecommendation = (e:React.ChangeEvent<HTMLInputElement>) => {
        let {value} = e.target;

        const isExists:boolean = this.state.recommendationLinks.filter((rl:IRecommendationLink) => {
            return rl.recommendation._id === value
        }).length > 0;
        let newRLList:IRecommendationLink[];

        if (isExists) {
            newRLList = this.state.recommendationLinks.filter((rl:IRecommendationLink) => {
                return rl.recommendation._id !== value
            });
        } else {
            newRLList = [...this.state.recommendationLinks, {
                recommendation: this.props.recommendations.filter((rec:IRecommendation) => rec._id === value)[0],
                min: 0.01,
                max: 1
            }]
        }

        this.setState({
            recommendationLinks: newRLList,
            searchRecItem: "",
            isSearchFieldFocused: false
        })
    }

    onBlurSearchField = (e:React.FocusEvent<HTMLDivElement>) => {      
        setTimeout(() => { 
            this.setState({ isSearchFieldFocused: false }) 
        }, 200);  
    }

    toggleRecommendationLinkItem = (e:React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        let dli:HTMLElement | null = e.currentTarget.parentElement;
        if (dli) {
            if (dli.classList.value.indexOf("active") === -1) {
                dli.classList.add("active");
            } else {
                dli.classList.remove("active");
            }
        }
    }

    onChangeRLICoef = (e:React.ChangeEvent<HTMLInputElement>) => {
        let {name, value, id} = e.target;
        let numValue:number = Number(value);
        numValue = numValue > 1 ? 1 : numValue;
        numValue = numValue < 0 ? 0 : numValue;
        this.setState({
            recommendationLinks: this.state.recommendationLinks.map((rl:IRecommendationLink) => {
                return rl.recommendation._id !== id ? rl : name === "max" ? {...rl, max: numValue} : {...rl, min: numValue};
            })
        });
    }

    saveRecommendationLinkItem = (e:React.MouseEvent<HTMLElement, MouseEvent>) => {
        let rli:HTMLElement | null | undefined = e.currentTarget.parentElement?.parentElement;
        if (rli) {
            if (rli.classList.value.indexOf("active") === -1) {
                rli.classList.add("active");
            } else {
                rli.classList.remove("active");
            }
        }
    }

    removeRecommendationLinkItem = (e:React.MouseEvent<HTMLElement, MouseEvent>, ind:number, _idRec:string) => {
        let newRLList:IRecommendationLink[] = this.state.recommendationLinks;
        newRLList.splice(ind, ind+1);

        console.log(this.state.foundRecList.filter((foundRec:IRecommendation) => {
            return foundRec._id !== _idRec
        }));
        
        this.setState({
            recommendationLinks: newRLList,
            foundRecList: this.state.foundRecList.filter((foundRec:IRecommendation) => {
                return foundRec._id !== _idRec
            })
        })
        e.stopPropagation();
    }

    _renderRecommendationLinks = () => {
        if (this.state.recommendationLinks.length === 0 || this.props.symptoms.length === 0) return (
            <div className="diagnoses-links__item"><p>Выберите рекомендацию</p></div>
        );
        return this.state.recommendationLinks.map((rl:IRecommendationLink, ind:number) => {
            
            return (
                <div 
                    key={ind} 
                    className="diagnoses-links__item active"
                >
                    <div 
                        className="diagnoses-links__item-header"
                        onClick={this.toggleRecommendationLinkItem}
                    >
                        <div className="circle">{rl.recommendation.title[0]}</div>
                        <h2>{rl.recommendation.title}</h2>
                        <p className="regular">
                            {rl.recommendation.symptoms.map((sym_id:string) => {
                                return this.props.symptoms.filter((sym:IStartSymptom) => {
                                    return sym._id === sym_id
                                })[0].title
                            }).join(", ")}
                        </p>

                        <button onClick={(e) => this.removeRecommendationLinkItem(e, ind, rl.recommendation._id || "")} className="btn-close"></button>
                        <button className="btn-info"></button>
                        <button className="btn-expand"></button>
                    </div>
                    <div className="diagnoses-links__item-body">
                        <div>
                            <input 
                                type="number" min="0" max="100" step="0.1"
                                id={rl.recommendation._id} name="max"
                                value={rl.max} onChange={this.onChangeRLICoef}
                            />
                            <p>
                            Введите вероятность (от 0 до 1) того, что у пациента <b> эта </b> 
                            болезнь, при положительном ответе на этот вопрос.
                            </p>
                        </div>
                        <div>
                            <input 
                                type="number" min="0" max="100" step="0.01"
                                id={rl.recommendation._id} name="min"
                                value={rl.min} onChange={this.onChangeRLICoef}
                            />
                            <p>
                            Введите вероятность (от 0 до 1) того, что у пациента 
                            <b> другая</b> болезнь, при положительном ответе на этот вопрос.
                            </p>
                        </div>
                        <button 
                            className="btn" 
                            data-type="ghost"
                            onClick={this.saveRecommendationLinkItem}
                        >сохранить</button>
                    </div>
                </div>
            )
        })
    }

    _renderFoundRecList = () => {
        if (this.state.foundRecList.length === 0) return (<p>Совпадений не найдено</p>)
        return this.state.foundRecList.map((item:IRecommendation, ind:number) => {
            const isChecked:boolean = this.state.recommendationLinks.filter((rl:IRecommendationLink) => rl.recommendation._id === item._id).length > 0;
            return (
                <div key={ind}>
                    <label className="chbx-container">
                        {item.title}
                        <input 
                            type="checkbox" 
                            name="foundRec" 
                            value={item._id} 
                            checked={isChecked}
                            onChange={this.onSelectRecommendation}
                        />
                        <span className="checkmark"></span>
                    </label>
                </div>
            );
        });
    }

    searchRecItems = (str:string) => {        
        this.setState({
            isSearchFieldFocused: true,
            foundRecList: this.props.recommendations.filter((rec:IRecommendation, ind:number) => {
                return rec.title.toLowerCase().indexOf(str.toLowerCase()) !== -1
            }).slice(0, 4)
        })
    }

    updateRec = async () => {
        for (let field in this.state.errors) {
            await this.validatingForm(field);          
        }

        if (this.state.isFormValid) {
            this.props.updateAction({
                title: this.state.title, 
                recommendationLinks: this.state.recommendationLinks,
                _id: this.state._id
            });
            this.setState(initialState)
        }
    }

    removeRec = () => {
        if (this.state._id) {
            this.props.removeAction(this.state._id);
            this.setState(initialState);
        }        
    }

    onActive = (q:IQuestion) => {        
        if (q) {
            this.setState({
                ...q
            });
        } else {
            this.setState(initialState);
        }
    }

    render() {        
        return (
            <>
                <h1>{this.state._id ? "Редактирование": "Создание"} вопроса</h1>
                <div className="fields">
                    <p>
                    Сформулируйте вопрос так, чтобы он выявлял 
                    какую-либо симптоматику.
                    </p>
                    <div className={`input-wrapper ${this.state.errors.title.length > 0 ? "error" : ""}`}>
                        <input 
                            type="text" 
                            placeholder="Напишите вопрос" 
                            name="title"
                            value={this.state.title}
                            onChange={this.onHandleChange}
                        />
                        <span>{this.state.errors.title}</span>
                    </div>
                </div>
                <div className="fields found-rec">
                    <p>
                    Если предположить, что вопрос выявляет симптом, а 
                    рекомендация подразумевает собой определенный диагноз, 
                    то выберите рекомендации (диагнозы), которые имеют 
                    выявляемый симптом.
                    </p>
                    <div className={`input-wrapper ${this.state.errors.recommendationLinks.length > 0 ? "error" : ""}`}>
                        <input 
                            type="text" 
                            placeholder="Введите наименование рекомендации (диагноза)" 
                            name="searchRecItem"
                            value={this.state.searchRecItem}
                            onChange={this.onHandleChange}
                            onFocus={this.onHandleChange}
                            onBlur={this.onBlurSearchField}
                        />
                        <span>{this.state.errors.recommendationLinks}</span>
                        <div 
                            className={`found-rec-list ${this.state.isSearchFieldFocused ? "active" : ""}`}
                        >
                            {this._renderFoundRecList()}
                        </div>
                    </div>
                    <div className="diagnoses-links">
                        <div className="diagnoses-links__header">
                            <p className="regular">Наименование</p>
                            <p className="regular">Первичный симптом</p>
                        </div>
                        {this._renderRecommendationLinks()}
                    </div>
                </div>
                <div className="actions">
                    <button onClick={this.updateRec} data-type="filled" className="btn">сохранить</button>
                    {this.state._id ? (
                        <button onClick={this.removeRec} data-type="ghost" className="btn">удалить</button>
                    ) : ""}  
                </div>
                
            </>
        )
    }
}

