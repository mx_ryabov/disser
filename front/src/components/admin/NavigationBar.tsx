import React, { Component } from 'react';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';

interface props extends RouteComponentProps {}

class NavigationBar extends Component<props, {}> {
    links:INavLink[] = [
        {title: "Рекомендации", src: "recomendations"},
        {title: "Вопросы", src: "questions"},
        {title: "Стартовые симптомы", src: "symptoms"},
        {title: "Ответы", src: "reviews"},
    ]
    render() {
        return (
            <nav>
                {this.links.map((link:INavLink, ind:number) => {
                    return (
                        <Link to={`${this.props.match.url}/${link.src}`} key={ind}>
                            <h1
                                className={this.props.location.pathname === `${this.props.match.url}/${link.src}` ? "active" : ""}
                            >
                                {link.title}
                            </h1>
                        </Link>
                    )
                })}
            </nav>
        )
    }
}

interface INavLink {
    title: string;
    src: string;
}


export default withRouter(NavigationBar);