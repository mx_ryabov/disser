import React, { Component, ComponentState } from 'react';
import { SYMPTOM_TYPE, IStartSymptom } from '../../store/admin/types';

interface props {
    updateAction: (title:string, type: SYMPTOM_TYPE, _id?:string) => void;
    removeAction: (_id:string) => void;
}

interface state {
    _id: string | undefined;
    title: string;
    type: SYMPTOM_TYPE;
    errors: {
        title: string;
    };
}

const initialState:state = {
    _id: undefined,
    title: "",
    type: "BODY_SYMPTOM" as SYMPTOM_TYPE,
    errors: {
        title: "",
    }
}

export default class StartSymptomForm extends Component<props, state> {
    state = initialState

    onHandleChange = (e:React.ChangeEvent<HTMLInputElement>) => {
        let {name, value} = e.target;
        this.setState(
            {[name] : value} as ComponentState
        );
        
    }

    updateSymptom = () => {
        this.props.updateAction(this.state.title, this.state.type, this.state._id);
        this.setState(initialState)
    }

    removeSymptom = () => {
        if (this.state._id) {
            this.props.removeAction(this.state._id);
            this.setState(initialState);
        }        
    }

    onActive = (sym:IStartSymptom | undefined) => {
        if (sym) {
            this.setState({
                ...sym
            });
        } else {
            this.setState(initialState);
        }
    }
    
    render() {
        return (
            <>
                <h1>{this.state._id ? "Редактирование": "Создание"} стартового симптома</h1>
                <div className="fields">
                    <div className={`input-wrapper ${this.state.errors.title.length > 0 ? "error" : ""}`}>
                        <input 
                            type="text" 
                            placeholder="Напишите наименование..." 
                            name="title"
                            value={this.state.title}
                            onChange={this.onHandleChange}
                        />
                        <span>{this.state.errors.title}</span>
                    </div>
                </div>
                <div className="fields">
                    <p>Выберите тип симптома</p>
                    <div className="radiobuttons">
                        <label className="radio-container">Часть тела
                            <input 
                                type="radio" 
                                name="type" 
                                value="BODY_SYMPTOM"
                                onChange={this.onHandleChange}
                                checked={this.state.type === "BODY_SYMPTOM"}
                            />
                            <span className="checkmark"></span>
                        </label>
                        <label className="radio-container">Недуг
                            <input 
                                type="radio" 
                                name="type" 
                                value="OTHER_SYMPTOM" 
                                onChange={this.onHandleChange}
                                checked={this.state.type === "OTHER_SYMPTOM"}
                            />
                            <span className="checkmark"></span>
                        </label>
                    </div>
                </div>
                <div className="actions">
                    <button onClick={this.updateSymptom} data-type="filled" className="btn">сохранить</button>
                    {this.state._id ? (
                        <button onClick={this.removeSymptom} data-type="ghost" className="btn">удалить</button>
                    ) : ""}                    
                </div>
            </>
        )
    }
}
