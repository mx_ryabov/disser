import React, { Component } from 'react';

interface props {
    firstName: string;
    lastName: string;
    position: string;
    isFetching: boolean;
    onLogout: () => Promise<void>;
}

export default class UserBar extends Component<props, {}> {
    render() {
        if (this.props.isFetching || !this.props.firstName || !this.props.lastName) return (<div></div>)
        return (
            <div className="user-bar">
                <div className="user-info">
                    <div className="avatar">{this.props.firstName[0] + this.props.lastName[0]}</div>
                    <div className="description">
                        <p className="regular">{this.props.lastName} {this.props.firstName}</p>
                        <span>{this.props.position}</span>
                    </div>
                </div>
                <button onClick={this.props.onLogout} className="exit-btn medium">Выход</button>
            </div>
        )
    }
}
